// 引入请求
import request from '@/utils/request'

// 获取spu数据的接口
export const reqSpuList = (page, limit, category3Id) => request({
    url: `/admin/product/${page}/${limit}`,
    method: 'get',
    params: { category3Id }
})

// 修改SPU | 添加SPU
export const reqAddOrUpdateSpu = (spuInfo) =>{
	// 携带参数带有ID 修改Spu
	if(spuInfo.id){
		return request({ url: '/admin/product/updateSpuInfo', method: 'post', data: spuInfo });
	}else{
		// 没有携带参数 不带ID 添加Spu
		return request({url:'/admin/product/saveSpuInfo', method: 'post', data: spuInfo})
	}
}
    // 获取SPU信息接口
    //  /admin/product/getSpuById/{spuId}   get
	export const reqSpu = (spuId) => request({
        url: `/admin/product/getSpuById/${spuId}`,
        method: 'get'
    })
    // 获取品牌信息接口
    //  /admin/product/baseTrademark/getTrademarkList  get
export const reqTradeMarkList = () => request({
        url: '/admin/product/baseTrademark/getTrademarkList',
        method: 'get'
    })
    // 获取SPU图片接口
    //  /admin/product/spuImageList/{spuId}  get
export const reqSpuImageList = (spuId) => request({
        url: `/admin/product/spuImageList/${spuId}`,
        method: 'get'
    })
    // 获取平台销售属性 整个平台销售属性一共三个
    //GET /admin/product/baseSaleAttrList
export const reqBaseSaleAttrList = () => request({
    url: '/admin/product/baseSaleAttrList',
    method: 'get'
})

