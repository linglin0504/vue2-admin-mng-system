import request from "@/utils/request";
// 获取品牌接口
export function getTradeMarkList(page, limit) {
  return request({
    url: `/admin/product/baseTrademark/getTrademarkList/${page}/${limit}`,
    method: "GET",
  });
}

// url:`/admin/product/baseTrademark/updatee`, // 接口地址
// method:'PUT', // 默认是get
// 封装品牌接口判断是添加还是修改
export const reqAddOrTradeMark = (tradeMark) => {
  if (tradeMark.id) {
    // 带有ID 修改操作
    return request({
      url: `/admin/product/baseTrademark/update`,
      method: "put",
      data: tradeMark,
    });
  } else {
    // 不带ID 添加操作
    return request({
      url: `/admin/product/baseTademark/save`,
      method: "post",
      data: tradeMark,
    });
  }
};
// 删除品牌接口
export const reqDeleteTradeMark = (id) =>
  request({
    url: `admin/product/baseTrademark/remove/${id}`,
    method: "delete",
  });
