import request from '@/utils/request'

export function login(data) { // 登录
  return request({
    url: '/admin/acl/index/login',
    method: 'post',
    data
  })
}

export function getInfo(token) { // 获取用户信息
  return request({
    url: '/admin/acl/index/info',
    method: 'get',
    params: { token }
  })
}

export function logout() { // 登出
  return request({
    url: '/admin/acl/index/logout',
    method: 'post'
  })
}
