import Vue from 'vue'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import locale from 'element-ui/lib/locale/lang/en' // lang i18n

import '@/styles/index.scss' // global css

import App from './App'
import store from './store'
import router from './router'

import '@/icons' // icon
import '@/permission' // permission control

if (process.env.NODE_ENV === 'production') {
    const { mockXHR } = require('../mock')
    mockXHR()
}

// set ElementUI lang to EN
Vue.use(ElementUI, { locale })
// 如果想要中文版 element-ui，按如下方式声明
// Vue.use(ElementUI)

Vue.config.productionTip = false
// 引入相关API请求接口函数
import API from '@/api' //引入API下的index.js
// 组件实例原型指向Vue的prototype
// 任意组件可以使用API相关接口
//
Vue.prototype.$API = API //全局注册组件
import CategorySelect from '@/components/CategorySelect' //引入API下的index.js
Vue.component(CategorySelect.name, CategorySelect) //全局注册组件
import HintButton from '@/components/HintButton' //引入API下的index.js
Vue.component(HintButton.name, HintButton) //全局注册组件


new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App)
})
