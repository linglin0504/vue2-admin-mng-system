import { reqCategory1List } from "@/api/product/category";
const state = {
  category1List: [],
};
const mutations = {
  SETCATEGORY1LIST(state, category1List) {
    state.category1List = category1List;
  },
};
const actions = {
  //  获取一级分类action
  async get_Category1List({ commit }) {
    let result = await reqCategory1List();
    commit("SETCATEGORY1LIST", result.data);
    console.log(result);
  },
};
export default {
  state,
  mutations,
  actions,
  namespaced: true,
};
